package com.cibertec.apividal.dto;

import javax.validation.constraints.NotNull;

public class EnvioMercanciaDTO {

  private String origen;
  private String destino;
  private String mercancia;

  @NotNull
  private Double pesoMercanciaKg;

  public EnvioMercanciaDTO(String origen, String destino, String mercancia, Double pesoMercanciaKg) {
    this.origen = origen;
    this.destino = destino;
    this.mercancia = mercancia;
    this.pesoMercanciaKg = pesoMercanciaKg;
  }

  public String getOrigen() {
    return origen;
  }

  public void setOrigen(String origen) {
    this.origen = origen;
  }

  public String getDestino() {
    return destino;
  }

  public void setDestino(String destino) {
    this.destino = destino;
  }

  public String getMercancia() {
    return mercancia;
  }

  public void setMercancia(String mercancia) {
    this.mercancia = mercancia;
  }

  public Double getPesoMercanciaKg() {
    return pesoMercanciaKg;
  }

  public void setPesoMercanciaKg(Double pesoMercanciaKg) {
    this.pesoMercanciaKg = pesoMercanciaKg;
  }
}
