package com.cibertec.apividal.controller;

import com.cibertec.apividal.dto.EnvioMercanciaDTO;
import com.cibertec.apividal.service.MercanciaService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class MercanciaController {

  private final MercanciaService mercanciaService;

  public MercanciaController(MercanciaService mercanciaService) {
    this.mercanciaService = mercanciaService;
  }

  @PostMapping("/apiVidal/glovo/mercancia")
  public ResponseEntity<String> obtenerCostoDeServicio(@RequestBody @Valid EnvioMercanciaDTO envioMercanciaDTO) {
    try {
      String resultado = mercanciaService.obtenerResultado(envioMercanciaDTO);
      return ResponseEntity.ok(resultado);
    } catch (Exception e) {
      return ResponseEntity.internalServerError().build();
    }
  }
}
