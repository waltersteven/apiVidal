package com.cibertec.apividal.controller;

import com.cibertec.apividal.service.MonedaEnum;
import com.cibertec.apividal.service.TipoDeCambioService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TipoDeCambioController {

  private final TipoDeCambioService tipoDeCambioService;

  public TipoDeCambioController(TipoDeCambioService tipoDeCambioService) {
    this.tipoDeCambioService = tipoDeCambioService;
  }

  @GetMapping("/apiVidal/bcr/tipoCambio/{codigoMoneda}")
  public ResponseEntity<String> obtenerTipoDeCambio(@PathVariable MonedaEnum codigoMoneda) {
    try {
      String valorEnSoles = tipoDeCambioService.obtenerTipoDeCambioEnSoles(codigoMoneda);
      return ResponseEntity.ok(valorEnSoles);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }
}
