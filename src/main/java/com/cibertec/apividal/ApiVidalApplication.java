package com.cibertec.apividal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiVidalApplication {

  public static void main(String[] args) {
    SpringApplication.run(ApiVidalApplication.class, args);
  }

}
