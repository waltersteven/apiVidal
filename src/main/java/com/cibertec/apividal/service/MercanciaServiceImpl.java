package com.cibertec.apividal.service;

import com.cibertec.apividal.dto.EnvioMercanciaDTO;
import org.springframework.stereotype.Service;

@Service
public class MercanciaServiceImpl implements MercanciaService {
  @Override
  public String obtenerResultado(EnvioMercanciaDTO envioMercanciaDTO) {
    TransporteEnum transporte = obtenerTransporte(envioMercanciaDTO.getPesoMercanciaKg());
    Double costo = obtenerCosto(transporte);

    return "El costo por el delivery es S/. [" + costo + "] y se transporta en [" + transporte.obtenerNombre() + "]";
  }

  @Override
  public Double obtenerCosto(TransporteEnum transporte) {
    return transporte.obtenerCosto();
  }

  @Override
  public TransporteEnum obtenerTransporte(Double pesoMercancia) {
    if (pesoMercancia > 15) {
      return TransporteEnum.AUTO;
    }

    return TransporteEnum.MOTO;
  }
}
