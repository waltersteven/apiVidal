package com.cibertec.apividal.service;

import com.cibertec.apividal.dto.EnvioMercanciaDTO;

public interface MercanciaService {
  String obtenerResultado(EnvioMercanciaDTO envioMercanciaDTO);
  Double obtenerCosto(TransporteEnum transporte);
  TransporteEnum obtenerTransporte(Double pesoMercancia);
}
