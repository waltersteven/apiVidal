package com.cibertec.apividal.service;

public interface TipoDeCambioService {
  String obtenerTipoDeCambioEnSoles(MonedaEnum moneda);
}
