package com.cibertec.apividal.service;

public enum TransporteEnum {
  MOTO("Moto", 10.00d),
  AUTO("Auto", 15.00d),
  ;

  private String nombre;
  private Double costo;

  TransporteEnum(String nombre, Double costo) {
    this.nombre = nombre;
    this.costo = costo;
  }

  public Double obtenerCosto() {
    return this.costo;
  }

  public String obtenerNombre() {
    return this.nombre;
  }

  public TransporteEnum buscarTransporte(String transporte) {
    for (TransporteEnum transporteEnum : TransporteEnum.values()) {
      if (transporteEnum.obtenerNombre().equalsIgnoreCase(transporte)) {
        return transporteEnum;
      }
    }

    return null;
  }

}
