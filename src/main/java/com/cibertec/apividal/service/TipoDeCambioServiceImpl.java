package com.cibertec.apividal.service;

import org.springframework.stereotype.Service;

@Service
public class TipoDeCambioServiceImpl implements TipoDeCambioService {
  @Override
  public String obtenerTipoDeCambioEnSoles(MonedaEnum moneda) {
    return "S/." + moneda.obtenerValorEnSoles();
  }
}
