package com.cibertec.apividal.service;

public enum MonedaEnum {
  USD("Dolar Americano", 3.25),
  EUR("Euro", 3.83),
  GBP("Libra esterlina", 4.28),
  ;

  private String nombre;
  private Double valorEnSoles;

  MonedaEnum(String nombre, Double valorEnSoles) {
    this.nombre = nombre;
    this.valorEnSoles = valorEnSoles;
  }

  public Double obtenerValorEnSoles() {
    return this.valorEnSoles;
  }
}
